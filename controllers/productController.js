const Product = require('../models/product.js')

module.exports.addProduct = (body) => {

	let newProduct = new Product({
		name: body.name,
		description: body.description,
		size: body.size,
		PCD: body.PCD,
		offset: body.offset,
		color: body.color,
		price: body.price,
		quantity: body.quantity
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false; 
		// save unsuccessful
		}else{
			return true;
		// save successful
		}
	})
}

module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		})		
}		


module.exports.getProduct = (params) => {
	return Product.findById(params.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		size: body.size,
		PCD: body.PCD,
		offset: body.offset,
		color: body.color,
		price: body.price,
		quantity: body.quantity
	}
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}


module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}