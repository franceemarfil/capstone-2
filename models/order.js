const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Name is required"]
	},
	orderedByUser: [
			{
				productId: {
					type: String
				},
				name: String,
				quantity: {
					type: Number,
					required: true
				},
				price: Number
			}
	],
	totalAmount: {
		type: Number,
		required: true
	},
	isOrdered: {
		type: Boolean,
		default: false
	},
	orderedOn: {
				type: Date,
				default: new Date()
			},
})

module.exports = mongoose.model("Order", orderSchema);