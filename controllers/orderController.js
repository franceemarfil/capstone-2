const Order = require('../models/order.js');
const Product = require('../models/product.js');
const User = require('../models/user.js')

// Order Products
module.exports.orderProducts = (body) => {
	let newOrder = new Order({
		name: body.name,
		orderedByUser: body.orderedByUser,
		totalAmount: body.totalAmount,
		email: body.email,
		isOrdered: body.isOrdered
	})

	return newOrder.save().then((order, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}



// Get User's cart
module.exports.getCart = (orderId) => {
	return Order.findById(orderId).then(result => {
		return result;
	})
}


// Get all orders
module.exports.getAllOrders = () => {
		return Order.find({isOrdered: true}).then(result => {
			return result;
		})		
}