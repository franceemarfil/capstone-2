// The dependencies
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController')
const auth = require("../auth")


// Route to create a new product
router.post("/", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
})

// Route to get all active products
router.get("/all", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.getAllActive(req.body).then(resultFromGetAllActive => res.send(resultFromGetAllActive))
	}else{
		res.send({auth: "Not an admin"})
	}	
}) 


// Route to get a specific product
router.get("/:productId", auth.verify, (req, res) => {
	// let token = auth.decode(req.headers.authorization)
	// if(token.isAdmin === true){
		productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
	}
	// else{
		res.send({auth: "Not an admin"})
	


// Route to add user to product
router.post("/productOrder", (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
	}
	productController.addedUser(data).then(resultFromAddedUser => res.send(resultFromAddedUser))	
})

// Route to update a product
router.put("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.updateProduct(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
	}else{
		res.send({auth: "Not an Admin"})
	}
})


// Route to archive a product
router.delete("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.archiveProduct(req.params).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an Admin"})
	}
})

module.exports = router;

// user <-> view/postman <-> http request/response <-> routes <-> controllers <-> models <-> database