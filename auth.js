const jwt = require("jsonwebtoken");
const secret = "capstone-2API"

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
	}
	return jwt.sign(data, secret, {})
}

// Verification of the token (Analogy: receive the gift and verify if the sender is legitimate and the gift was not tampered with)
module.exports.verify = (req, res, next) => {
	// get Our JSONWebToken is found in the authorization header
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		console.log(token)
		// slice removes the unnecessary "Bearer:" part of the token
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: "failed"})
			}else{
				next();
			}
		})
	}else{
		// if the token is not present
		return res.send({auth: "failed"})
	}
}

// Decoding of the token (analogy: open the gift and get the contents)
module.exports.decode = (token) => {
	// Check if token is present
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

			return jwt.verify(token, secret, (err, data) => {
				if(err){
					return null;
				}else{
					return jwt.decode(token, {complete: true}).payload
					// complete: true means do a complete decoding of the token
					// payload is the data from the token when we created the access token
				}
			})
	}else{
		// no token
		return null;
	}
}