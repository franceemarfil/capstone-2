const User = require("../models/user");
const Product = require("../models/product");
const auth = require("../auth");
const bcrypt = require("bcrypt");


// Registration
module.exports.registerUser = (body) => {
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		mobileNumber: body.mobileNumber,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		address: body.address
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


// Authenticate or Log In
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false;
			// User doesn't exist
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return false;
				// can't log in
			}
		}
	})
}

// Set user as admin
module.exports.admin = (params) => {
	let updatedAdmin = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(params.userId, updatedAdmin).then((user, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

// Order or Check Out of products
module.exports.checkout = async (data) => {

	let userSaveStatus = await User.findById(data.userId).then(user => {
		user.productOrders.push({productId: data.productId})
		return user.save().then((user, err) => {
			if(err){
				return false;
				// product not saved in user's productOrders
			}else{
				return true;
				// product successfully saved in user's productOrders
			}
		})
	})

	let productSaveStatus = await Product.findById(data.productId).then(product => {
		product.orderedByUser.push({userId: data.userId})
		return product.save().then((product, err) => {
			if(err){
				return false;
				// user not saved in user's orderedByUser
			}else{
				return true;
				// user successfully saved in orderedByUser
			}
		})
	})

	if(userSaveStatus && productSaveStatus){
		return true;
	}else{
		return false;
	}
}


// Getting user's cart orders
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined;
		return result;
	})
}