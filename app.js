// The dependencies
const express = require("express");
const mongoose = require("mongoose");

//The Import routes
const productRoutes = require('./routes/productRoutes');
const userRoutes = require('./routes/userRoutes');
const orderRoutes = require('./routes/orderRoutes');

// The Server setup
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}))

// The link routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


// To connect with MongoDB Atlas
mongoose.connect("mongodb+srv://admin1:admin12345@cluster0.hw9hx.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Confirmation that it is connected to Atlas
mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas.")
})

// Shows that API actually runs
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
})