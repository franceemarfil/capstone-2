const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	size: {
		type: String
		// default: [true, "Size is required"]
	},
	PCD: {
		type: String
		// default: [true, "PCD is required"]
	},
	offset: {
		type: String
		// default: [true, "Offset is required"]
	},
	color: {
		type: String
		// default: [true, "Color is required"]
	},
	price: {
		type: Number
		// required: [true, "Price is required"]
	},
	quantity: {
		type: Number
		// default: [true, "Quantity is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	shipping: {
		type: String,
		enum: ['JRSExpress', 'LBC']
	},
	paymentMethod: {
		type: Number,
		enum: ['Cash', 'Credit Card']
	},
	orderedByUser: [
		{
			userId: {
				type: String
				// required: [true, "UserId is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},
			quantity: {
				type: Number
				// default: [true, "Quantity is required"]
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);