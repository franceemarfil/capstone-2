const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController")

// Route for registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})


// Route for logging in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})


// Route to set user as admin
router.put("/update/:userId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		userController.admin(req.params).then(resultFromUpdatedAdmin => res.send(resultFromUpdatedAdmin))
	}else{
		res.send({auth: "Not an admin"})
	}
})


// Route for users ordering products
router.post("/checkout", (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		price: req.body.price
	}
	userController.checkout(data).then(resultFromOrder => res.send(resultFromOrder))	
})


// Route for getting user's cart orders
router.get("/cart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})


module.exports = router



// EXAMPLE ROUTE WITHOUT ADMIN VERIFICATION
// router.post('/', (req, res) => {
// 		courseController.addCourse(req.body).then(resultFromAddCourse => res.send(resultFromAddCourse))	
// }