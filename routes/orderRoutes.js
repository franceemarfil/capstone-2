// The dependencies
const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController')
const auth = require("../auth")

// Order Products
router.post("/orderProducts", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.orderProducts(req.body).then(resultFromOrder => res.send(resultFromOrder))
})

// Get the User's cart
router.get("/myCart/:orderId", (req, res) => {
	orderController.getCart(req.orderId).then(resultFromGetUsersCart => res.send(resultFromGetUsersCart))
})

// Get the All Product Orders
router.get("/allProductOrders", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		orderController.getAllOrders(req.body).then(resultFromGetAllOrders => res.send(resultFromGetAllOrders))
	}else{
		res.send({auth: "Not an admin"})
	}
}) 


module.exports = router;